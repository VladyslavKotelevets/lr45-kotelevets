<%--
  Created by IntelliJ IDEA.
  User: Владелец
  Date: 24.05.2024
  Time: 0:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List, kotelevets.lr45.model.Laptop" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Список ноутбуків</title>
    <link rel="stylesheet" href="static/css/style.css">
</head>
<body>
<div class="background"></div>
<div class="content">
    <h1>Список ноутбуків</h1>
    <a href="index.jsp" class="btn">Головна</a>
    <a href="LaptopController?action=insert" class="btn">Додати ноутбук</a>
    <table class="table">
        <tr>
            <th>ID</th>
            <th>Виробник</th>
            <th>Модель</th>
            <th>Тип процесора</th>
            <th>Обсяг ОЗП (ГБ)</th>
            <th>Обсяг жорсткого диска (ГБ)</th>
            <th>Діагональ екрану (дюйм)</th>
            <th>Ціна (грн)</th>
            <th>Прізвище користувача</th>
            <th>Email користувача</th>
            <th>Дата</th>
            <th>Дії</th>
        </tr>
        <%
            List<Laptop> laptops = (List<Laptop>) request.getAttribute("laptops");
            for (Laptop laptop : laptops) {
        %>
        <tr class="testcolor">
            <td><%= laptop.getId() %></td>
            <td><%= laptop.getManufacturer() %></td>
            <td><%= laptop.getModel() %></td>
            <td><%= laptop.getProcessorType() %></td>
            <td><%= laptop.getRamCapacity() %></td>
            <td><%= laptop.getDiskCapacity() %></td>
            <td><%= laptop.getScreenDiagonal() %></td>
            <td><%= laptop.getPrice() %></td>
            <td><%= laptop.getUserLastname() %></td>
            <td><%= laptop.getUserEmail() %></td>
            <td><%= laptop.getDate() %></td>
            <td>
                <a href="LaptopController?action=edit&laptopId=<%= laptop.getId() %>">Редагувати</a>
                <a href="LaptopController?action=delete&laptopId=<%= laptop.getId() %>">Видалити</a>
            </td>
        </tr>
        <% } %>
    </table>
</div>
</body>
</html>
