<%--
  Created by IntelliJ IDEA.
  User: Владелец
  Date: 24.05.2024
  Time: 0:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String errorMessage = (String) request.getAttribute("errorMessage");
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Додати/редагувати ноутбук</title>
    <link rel="stylesheet" href="static/css/style.css">
</head>
<body>
    <div class="background"></div>
    <div class="content">
        <h1>Додати/редагувати ноутбук</h1>
        <% if (errorMessage != null) { %>
        <div class="error-message"><%= errorMessage %></div>
        <% } %>
        <a href="index.jsp" class="btn">Головна</a>
        <a href="LaptopController?action=showAll" class="btn">Переглянути ноутбуки</a>
        <form action="LaptopController" method="post">
            <input type="hidden" name="laptopId" value="${laptop.id}">
            <label for="manufacturer">Виробник:</label>
            <input type="text" id="manufacturer" name="manufacturer" value="${laptop.manufacturer}">
            <label for="model">Модель:</label>
            <input type="text" id="model" name="model" value="${laptop.model}">
            <label for="processorType">Тип процесора:</label>
            <input type="text" id="processorType" name="processorType" value="${laptop.processorType}">
            <label for="ramCapacity">Обсяг ОЗП (ГБ):</label>
            <input type="number" id="ramCapacity" name="ramCapacity" value="${laptop.ramCapacity}">
            <label for="diskCapacity">Обсяг жорсткого диска (ГБ):</label>
            <input type="number" id="diskCapacity" name="diskCapacity" value="${laptop.diskCapacity}">
            <label for="screenDiagonal">Діагональ екрану (дюйм):</label>
            <input type="number" id="screenDiagonal" name="screenDiagonal" value="${laptop.screenDiagonal}">
            <label for="price">Ціна (грн):</label>
            <input type="number" id="price" name="price" value="${laptop.price}">
            <label for="userLastName">Прізвище користувача:</label>
            <input type="text" id="userLastName" name="userLastName" value="${laptop.userLastName}">
            <label for="userEmail">Email користувача:</label>
            <input type="email" id="userEmail" name="userEmail" value="${laptop.userEmail}">
            <label for="date">Дата:</label>
            <input type="text" id="date" name="date" value="${laptop.date}">
            <button type="submit" class="btn">Зберегти</button>
        </form>
    </div>
</body>
</html>

