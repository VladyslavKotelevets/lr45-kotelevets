package kotelevets.lr45.dao;

import kotelevets.lr45.model.Laptop;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class LaptopDAO implements LaptopDAOInterface<Laptop, Integer> {
    private Session currentSession;
    private Transaction currentTransaction;

    public LaptopDAO() {
    }

    public Session openCurrentSession() {
        currentSession = getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionwithTransaction() {
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession() {
        currentSession.close();
    }

    public void closeCurrentSessionwithTransaction() {
        currentTransaction.commit();
        currentSession.close();
    }

    public SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration().configure("META-INF/hibernate.cfg.xml");
        configuration.addAnnotatedClass(Laptop.class);
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
        SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
        return sessionFactory;
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    public void setCurrentTransaction(Transaction transaction) {
        this.currentTransaction = currentTransaction;
    }

    @Override
    public void persist(Laptop entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Laptop entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Laptop findById(Integer id) {
        Laptop laptop = (Laptop) getCurrentSession().get(Laptop.class, id);
        return laptop;
    }

    @Override
    public void delete(Laptop entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<Laptop> findAll() {
        List<Laptop> laptops = (List<Laptop>) getCurrentSession().createQuery("from Laptop").list();
        return laptops;
    }

    @Override
    public void deleteAll() {
        List<Laptop> entityList = findAll();
        for (Laptop entity : entityList) {
            delete(entity);
        }
    }
}
