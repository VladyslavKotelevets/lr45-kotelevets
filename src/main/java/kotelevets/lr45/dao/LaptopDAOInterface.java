package kotelevets.lr45.dao;

import kotelevets.lr45.model.Laptop;

import java.io.Serializable;
import java.util.List;

public interface LaptopDAOInterface<T, Id extends Serializable> {
    void persist(T entity);

    void update(T entity);

    T findById(Id id);

    void delete(T entity);

    List<T> findAll();

    void deleteAll();
}
