package kotelevets.lr45.util;

import kotelevets.lr45.model.Laptop;
import kotelevets.lr45.model.LaptopModel;

public class converter {
    public static void laptopModelToEntity(Laptop laptop, LaptopModel model) {
        laptop.setManufacturer(model.getManufacturer());
        laptop.setModel(model.getModel());
        laptop.setProcessorType(model.getProcessorType());
        laptop.setRamCapacity(model.getRamCapacity());
        laptop.setDiskCapacity(model.getDiskCapacity());
        laptop.setScreenDiagonal(model.getScreenDiagonal());
        laptop.setPrice(model.getPrice());
        laptop.setUserLastName(model.getUserLastName());
        laptop.setUserEmail(model.getUserEmail());
        laptop.setDate(model.getDate());
    }
}
