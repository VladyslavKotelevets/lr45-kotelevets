package kotelevets.lr45.model;

import jakarta.persistence.*;

@Entity
@Table(name = "laptops")
public class Laptop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "manufacturer", nullable = true)
    private String manufacturer;

    @Column(name = "model", nullable = true)
    private String model;

    @Column(name = "processor_type", nullable = true)
    private String processorType;

    @Column(name = "ram_capacity", nullable = true)
    private Integer ramCapacity;

    @Column(name = "disk_capacity", nullable = true)
    private Integer diskCapacity;

    @Column(name = "screen_diagonal", nullable = true)
    private Float screenDiagonal;

    @Column(name = "price", nullable = true)
    private Float price;

    @Column(name = "user_lastname", nullable = true)
    private String userLastname;

    @Column(name = "user_email", nullable = true)
    private String userEmail;

    @Column(name = "date", nullable = true)
    private String date;

    public Laptop(String manufacturer, String model, String processorType, Integer ramCapacity, Integer diskCapacity,
                  Float screenDiagonal, Float price, String userLastname, String userEmail, String date) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.processorType = processorType;
        this.ramCapacity = ramCapacity;
        this.diskCapacity = diskCapacity;
        this.screenDiagonal = screenDiagonal;
        this.price = price;
        this.userLastname = userLastname;
        this.userEmail = userEmail;
        this.date = date;
    }

    public Laptop() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProcessorType() {
        return processorType;
    }

    public void setProcessorType(String processorType) {
        this.processorType = processorType;
    }

    public Integer getRamCapacity() {
        return ramCapacity;
    }

    public void setRamCapacity(Integer ramCapacity) {
        this.ramCapacity = ramCapacity;
    }

    public Integer getDiskCapacity() {
        return diskCapacity;
    }

    public void setDiskCapacity(Integer diskCapacity) {
        this.diskCapacity = diskCapacity;
    }

    public Float getScreenDiagonal() {
        return screenDiagonal;
    }

    public void setScreenDiagonal(Float screenDiagonal) {
        this.screenDiagonal = screenDiagonal;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getUserLastname() {
        return userLastname;
    }

    public void setUserLastName(String userLastname) {
        this.userLastname = userLastname;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Laptop{" +
                "id=" + id +
                ", manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", processorType='" + processorType + '\'' +
                ", ramCapacity=" + ramCapacity +
                ", diskCapacity=" + diskCapacity +
                ", screenDiagonal=" + screenDiagonal +
                ", price=" + price +
                ", userLastname='" + userLastname + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
