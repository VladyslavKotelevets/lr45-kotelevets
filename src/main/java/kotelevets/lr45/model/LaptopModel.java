package kotelevets.lr45.model;

public class LaptopModel {
    private Integer id;
    private String manufacturer;
    private String model;
    private String processorType;
    private Integer ramCapacity;
    private Integer diskCapacity;
    private Float screenDiagonal;
    private Float price;
    private String userLastName;
    private String userEmail;
    private String date;

    public LaptopModel(String manufacturer, String model, String processorType, Integer ramCapacity,
                       Integer diskCapacity, Float screenDiagonal, Float price, String userLastName,
                       String userEmail, String date) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.processorType = processorType;
        this.ramCapacity = ramCapacity;
        this.diskCapacity = diskCapacity;
        this.screenDiagonal = screenDiagonal;
        this.price = price;
        this.userLastName = userLastName;
        this.userEmail = userEmail;
        this.date = date;
    }

    public LaptopModel() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getProcessorType() {
        return processorType;
    }

    public void setProcessorType(String processorType) {
        this.processorType = processorType;
    }

    public Integer getRamCapacity() {
        return ramCapacity;
    }

    public void setRamCapacity(Integer ramCapacity) {
        this.ramCapacity = ramCapacity;
    }

    public Integer getDiskCapacity() {
        return diskCapacity;
    }

    public void setDiskCapacity(Integer diskCapacity) {
        this.diskCapacity = diskCapacity;
    }

    public Float getScreenDiagonal() {
        return screenDiagonal;
    }

    public void setScreenDiagonal(Float screenDiagonal) {
        this.screenDiagonal = screenDiagonal;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "LaptopModel{" +
                "id=" + id +
                ", manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", processorType='" + processorType + '\'' +
                ", ramCapacity=" + ramCapacity +
                ", diskCapacity=" + diskCapacity +
                ", screenDiagonal=" + screenDiagonal +
                ", price=" + price +
                ", userLastName='" + userLastName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
