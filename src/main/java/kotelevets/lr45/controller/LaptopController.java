package kotelevets.lr45.controller;

import kotelevets.lr45.util.converter;
import kotelevets.lr45.model.Laptop;
import kotelevets.lr45.model.LaptopModel;
import kotelevets.lr45.service.LaptopService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/LaptopController")
public class LaptopController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static String INSERT_OR_EDIT = "/insert.jsp";
    private static String LIST_LAPTOP = "/showAll.jsp";
    private LaptopService laptopService;

    public LaptopController() {
        super();
        laptopService = new LaptopService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        List<Laptop> laptopList = new ArrayList<>();

        if (action.equalsIgnoreCase("delete")) {
            int laptopId = Integer.parseInt(request.getParameter("laptopId"));
            laptopService.delete(laptopId);
            forward = LIST_LAPTOP;
            laptopList = laptopService.findAll();
            request.setAttribute("laptops", laptopList);
        } else if (action.equalsIgnoreCase("edit")) {
            forward = INSERT_OR_EDIT;
            int laptopId = Integer.parseInt(request.getParameter("laptopId"));
            Laptop laptop = laptopService.findById(laptopId);
            request.setAttribute("Laptop", laptopList);
        } else if (action.equalsIgnoreCase("showAll")) {
            forward = LIST_LAPTOP;
            laptopList = laptopService.findAll();
            request.setAttribute("laptops", laptopList);
        } else {
            forward = INSERT_OR_EDIT;
            request.setAttribute("laptops", laptopList);
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LaptopModel laptopModel = new LaptopModel();
        laptopModel.setManufacturer(request.getParameter("manufacturer"));
        laptopModel.setModel(request.getParameter("model"));
        laptopModel.setProcessorType(request.getParameter("processorType"));
        laptopModel.setRamCapacity(Integer.parseInt(request.getParameter("ramCapacity")));
        laptopModel.setDiskCapacity(Integer.parseInt(request.getParameter("diskCapacity")));
        laptopModel.setScreenDiagonal(Float.parseFloat(request.getParameter("screenDiagonal")));
        laptopModel.setPrice(Float.parseFloat(request.getParameter("price")));
        laptopModel.setUserLastName(request.getParameter("userLastName"));
        laptopModel.setUserEmail(request.getParameter("userEmail"));
        laptopModel.setDate(request.getParameter("date"));

        String laptopId = request.getParameter("laptopId");
        if (laptopId == null || laptopId.isEmpty()) {
            Laptop laptopEntity = new Laptop();
            converter.laptopModelToEntity(laptopEntity, laptopModel);
            laptopService.persist(laptopEntity);
        } else {
            laptopModel.setId(Integer.parseInt(laptopId));
            Laptop laptopEntity = laptopService.findById(laptopModel.getId());
            if (laptopEntity == null) {
                request.setAttribute("errorMessage", "Laptop not found.");
                RequestDispatcher view = request.getRequestDispatcher(LIST_LAPTOP);
                view.forward(request, response);
                return;
            }
            converter.laptopModelToEntity(laptopEntity, laptopModel);
            laptopService.update(laptopEntity);
        }
        List<Laptop> laptopList = laptopService.findAll();
        request.setAttribute("laptops", laptopList);

        RequestDispatcher view = request.getRequestDispatcher(LIST_LAPTOP);
        view.forward(request, response);
    }
}
