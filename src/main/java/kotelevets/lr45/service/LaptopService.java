package kotelevets.lr45.service;

import kotelevets.lr45.dao.LaptopDAO;
import kotelevets.lr45.model.Laptop;

import java.util.List;

public class LaptopService {
    private static LaptopDAO laptopDAO;

    public LaptopService() {
        laptopDAO = new LaptopDAO();
    }

    public void persist(Laptop entity) {
        laptopDAO.openCurrentSessionwithTransaction();
        laptopDAO.persist(entity);
        laptopDAO.closeCurrentSessionwithTransaction();
    }

    public void update(Laptop entity) {
        laptopDAO.openCurrentSessionwithTransaction();
        laptopDAO.update(entity);
        laptopDAO.closeCurrentSessionwithTransaction();
    }

    public Laptop findById(Integer id) {
        laptopDAO.openCurrentSession();
        Laptop laptop = laptopDAO.findById(id);
        laptopDAO.closeCurrentSession();
        return laptop;
    }

    public void delete(Integer id) {
        laptopDAO.openCurrentSessionwithTransaction();
        Laptop laptop = laptopDAO.findById(id);
        laptopDAO.delete(laptop);
        laptopDAO.closeCurrentSessionwithTransaction();
    }

    public List<Laptop> findAll() {
        laptopDAO.openCurrentSession();
        List<Laptop> laptopList = laptopDAO.findAll();
        laptopDAO.closeCurrentSession();
        return laptopList;
    }

    public void deleteAll() {
        laptopDAO.openCurrentSessionwithTransaction();
        laptopDAO.deleteAll();
        laptopDAO.closeCurrentSessionwithTransaction();
    }

    public LaptopDAO laptopDAO() {
        return laptopDAO;
    }
}
