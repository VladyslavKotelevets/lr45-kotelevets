package kotelevets.lr45;

import kotelevets.lr45.model.Laptop;
import kotelevets.lr45.service.LaptopService;

import java.util.List;

public class App {

    public static void main(String[] args) {
        LaptopService laptopService = new LaptopService();

        List<Laptop> laptopList = laptopService.findAll();
        // Виведення всіх записів з бази даних
        System.out.println("Усі ноутбуки:");
        for (var laptop : laptopList) {
            System.out.println(laptop);
        }

        var laptop = laptopService.findById(1);
        System.out.println(laptop);
    }
}
